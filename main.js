let _connection;
let partner_ready = false
let scene_number = 0
let scenes = []
let logged = false
let assets = {}
let clickables = []
let gotCode = false
let session_code
let partner_id
let landing_page
let code_page
let fontHeight = 18;
let touchBegin
let y_scroll_begin
let x_scroll_begin
let amihost = false
let audioStarted = false
let connectionDataCallbacks;

function emitRandomWord() {
    rndword = RiTa.pluralize(RiTa.randomWord("nn"))
    if (amihost) {
        scenes[2].host_random_word = rndword
    } else {
        scenes[2].guest_random_word = rndword
    }
    _connection.send({ type: "randomWord", partner_id: partner_id, word: rndword })
    setTimeout(() => {
        if (scene_number == 2) {
            scene_number = 3
        }
        setTimeout(() => {
            if (scene_number == 3) {
                scene_number = 4
                assets.bg_scene_2.stop()
                scenes[4].setup()
            }
        }, 5000)
    }, 15000)
}

document.addEventListener("DOMContentLoaded", function (ev) {
    // ROUTING

    var arr = window.location.search.split('?');

    if (arr.length > 1 && arr[1] !== '') {
        // CLIENT route
        document.getElementById("template-container").append(
            document.importNode(
                document.querySelector('#client-template').content,
                true
            )
        )

        // create Peer
        let peer = new Peer({ debug: 2 });

        peer.on("error", function (err) {
            console.log(err.type)

        })

        // peerserver connection callback
        peer.on("open", function (id) {
            console.log('My peer ID is: ' + id);
            console.log("Partner peer ID is: " + arr[1])

            // connect to peer
            _connection = peer.connect(arr[1], { serialization: "json" });
            console.log(_connection)

            // connection callback
            _connection.on("open", function () {

                // init p5
                let myp5 = new p5(s);
                document.getElementById("template-container").hidden = true
                document.getElementById("p5_loading").hidden = false

                console.log("Connesso a: " + _connection.peer)

                let connectionlabel = document.getElementById("connection-label")
                connectionlabel.innerHTML = "Connected"
                connectionlabel.style.color = "green"

                connectionDataCallbacks()

            })


            _connection.on("close", function () {
                let connectionlabel = document.getElementById("connection-label");
                connectionlabel.innerHTML = "Not connected";
                connectionlabel.style.color = "Red";
                location.reload();
            })
        })
    } else {
        // HOST route
        document.getElementById("template-container").append(
            document.importNode(
                document.querySelector('#host-template').content,
                true
            )
        )

        amihost = true

        // create Peer
        let peer = new Peer({ debug: 2 });

        peer.on("error", function (err) {
            console.log(err.type)
            peer = new Peer(peer.id)
            // peerserver connection callback
            peer.on("open", function (id) {

                // generate link
                console.log('My peer ID is: ' + id);
                document.getElementById("partnerlink").innerHTML = window.location + "?" + id;

                // connection callback
                peer.on("connection", function (conn) {
                    _connection = conn;
                    // Init p5
                    let myp5 = new p5(s);
                    document.getElementById("template-container").hidden = true
                    document.getElementById("p5_loading").hidden = false

                    console.log("Connesso a: " + conn.peer)

                    let connectionlabel = document.getElementById("connection-label")
                    connectionlabel.innerHTML = "Connected"
                    connectionlabel.style.color = "green"

                    connectionDataCallbacks()

                    _connection.on("close", function () {
                        let connectionlabel = document.getElementById("connection-label");
                        connectionlabel.innerHTML = "Not connected";
                        connectionlabel.style.color = "Red";
                    })
                })
            })
        })

        // peerserver connection callback
        peer.on("open", function (id) {

            // generate link
            console.log('My peer ID is: ' + id);
            document.getElementById("partnerlink").innerHTML = window.location + "?" + id;

            // connection callback
            peer.on("connection", function (conn) {
                _connection = conn;
                // Init p5
                let myp5 = new p5(s);
                document.getElementById("template-container").hidden = true
                document.getElementById("p5_loading").hidden = false

                console.log("Connesso a: " + conn.peer)

                let connectionlabel = document.getElementById("connection-label")
                connectionlabel.innerHTML = "Connected"
                connectionlabel.style.color = "green"

                connectionDataCallbacks()

                _connection.on("close", function () {
                    let connectionlabel = document.getElementById("connection-label");
                    connectionlabel.innerHTML = "Not connected";
                    connectionlabel.style.color = "Red";
                })
            })
        })

    }
})

// --------------------------------------------------------------

// P5 SKETCH

const s = (p) => {


    connectionDataCallbacks = function () {
        _connection.on("data", function (data) {
            if (data.type == "imready") {
                partner_ready = true
            }
            if (data.type == "letsbegin") {
                logged = true
                scenes[0].counter = 0
            }
            if (data.type == "someonetouch") {
                scenes[0].hi_bubble_showing = true
                assets.hi_sound[Math.floor(Math.random() * assets.hi_sound.length)].start()
                setTimeout(() => { scenes[0].hi_bubble_showing = false }, 250)
                if (!scenes[0].transitioning) {
                    scenes[0].transitioning = true
                    setTimeout(() => {
                        assets.hi_crescendo.start()
                    }, 3000)
                    setTimeout(() => {
                        scenes[0].transition()
                        setTimeout(() => {
                            scene_number = 1
                            assets.bg_scene_1.loop = true
                            assets.bg_scene_1.start()
                            assets.bg_scene_0.stop()
                            assets.hi_crescendo.stop()
                        }, 10000)
                    }, 1000)

                }
            }
            if (data.type == "onFlashlight") {
                scenes[1].partner_flashlight_on = true
                scenes[1].partner_flashlight_coords = [data.touchX, data.touchY]
                assets.clicks_torcia[Math.floor(Math.random() * assets.clicks_torcia.length)].start()
            }
            if (data.type == "moveFlashlight") {
                scenes[1].partner_flashlight_coords = [data.touchX, data.touchY]
            }
            if (data.type == "offFlashlight") {
                scenes[1].partner_flashlight_on = false
            }
            if (data.type == "flashlightOnInsect") {
                setTimeout(() => {
                    if (!scenes[1].insect_animating) {
                        assets.scarafaggi_loop.loop = true
                        assets.scarafaggi_loop.start()
                        setTimeout(() => {
                            scene_number = 2
                            assets.scarafaggi_loop.stop()
                            assets.bg_scene_1.stop()
                            assets.bg_scene_2.loop = true
                            assets.bg_scene_2.start()
                            //emitRandomWord()
                            assets.insectCrowd.hide()
                        }, 20000)
                        scenes[1].insect_animating = true
                    }
                }, 500)
            }
            if (data.type == "randomWord") {
                assets.pages_turn[Math.floor(Math.random() * assets.pages_turn.length)].start()
                if (amihost) {
                    scenes[2].guest_random_word = data.word
                } else {
                    scenes[2].host_random_word = data.word
                }
                if (!scenes[2].transitioning) {
                    scenes[2].transitioning = true
                    setTimeout(() => {
                        if (scene_number == 2) {
                            scene_number = 3
                        }
                        setTimeout(() => {
                            if (scene_number == 3) {
                                scene_number = 4
                                assets.bg_scene_2.stop()
                                scenes[4].setup()
                            }
                        }, 5000)
                    }, 15000)
                }

            }
            if (data.type == "onNewDot") {
                if (amihost) {
                    if (scenes[4].dots.guest.length < 1 || (data.touchX > scenes[4].dots.guest[scenes[4].dots.guest.length - 1].x)) {
                        scenes[4].dots.guest.push({ x: data.touchX, y: data.touchY })
                        if (data.touchX > p.textWidth(scenes[4].main_phrase.text)) {
                            scenes[4].animatedDots.guest.push(new AnimatedDot(data.touchX, data.touchY, p.frameCount))
                        }
                    }
                } else {
                    if (scenes[4].dots.host.length < 1 || (data.touchX > scenes[4].dots.host[scenes[4].dots.host.length - 1].x)) {
                        scenes[4].dots.host.push({ x: data.touchX, y: data.touchY })
                        if (data.touchX > p.textWidth(scenes[4].main_phrase.text)) {
                            scenes[4].animatedDots.host.push(new AnimatedDot(data.touchX, data.touchY, p.frameCount))
                        }
                    }
                }
            }
            if (data.type == "endScene4") {
                if (!scenes[4].ending) {
                    scenes[4].ending = true
                    setTimeout(() => {
                        if (scene_number == 4) {
                            scene_number = 5
                            assets.crescendo_tremore.start()
                            scenes[5].setup()
                        }
                    }, 10000)
                }
            }
            if (data.type == "endScene7") {
                setTimeout(() => {
                    if (scene_number == 7 && !scenes[7].ending) {
                        scenes[7].ending = true
                        scene_number = 8
                        scenes[8].setup()
                    }
                }, 10000)
            }
        })
    }

    // PRELOAD --------------------------------------------------------------

    p.preload = function () {
        console.log("loading")
        assets['instructions'] = p.loadImage("./assets/istruzioni.png")
        assets['title'] = p.loadImage("./assets/titolo.png")
        assets['fullscreen_icon'] = p.loadImage("./assets/fullscreen_icon.png")
        assets['jacquesFrancois'] = p.loadFont('./assets/JacquesFrancois-Regular.otf')
        assets['jacquesFrancoisShadow'] = p.loadFont('./assets/JacquesFrancoisShadow-Regular.otf')
        assets['hi_bubble'] = p.loadImage("./assets/hi_bubble.png")
        assets['someone'] = p.loadImage("./assets/someone.png")
        assets['flashlight'] = p.loadImage("./assets/flashlight.png")
        assets['insect'] = p.loadGif("./assets/onebugwalking.gif")
        assets['hi_sound'] = [
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_5.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_6.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_7.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_0/hi_samples/hi_8.wav").toDestination()
        ]
        assets['bg_scene_0'] = new Tone.Player("./assets/sounds/Scena_0/BG_noise.mp3").toDestination();
        assets['hi_crescendo'] = new Tone.Player("./assets/sounds/Scena_0/HI_crescendo.mp3").toDestination();
        assets['bg_scene_1'] = new Tone.Player("./assets/sounds/Scena_1/BG_natura.mp3").toDestination();
        assets['scarafaggi_loop'] = new Tone.Player("./assets/sounds/Scena_1/Scarafaggi_loop.mp3").toDestination();
        assets['clicks_torcia'] = [
            new Tone.Player("./assets/sounds/Scena_1/Clicks_torcia/Click_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_1/Clicks_torcia/Click_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_1/Clicks_torcia/Click_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_1/Clicks_torcia/Click_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_1/Clicks_torcia/Click_5.wav").toDestination(),
        ]
        assets['bg_scene_2'] = new Tone.Player("./assets/sounds/Scena_2/BG_dizionario.mp3").toDestination();
        assets['pages_turn'] = [
            new Tone.Player("./assets/sounds/Scena_2/pages_flip/page_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_2/pages_flip/page_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_2/pages_flip/page_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_2/pages_flip/page_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_2/pages_flip/page_5.wav").toDestination()
        ]
        assets['notes_p1'] = [
            new Tone.Player("./assets/sounds/Scena_3/p1_note/p1_note_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p1_note/p1_note_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p1_note/p1_note_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p1_note/p1_note_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p1_note/p1_note_5.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p1_note/p1_note_6.wav").toDestination()
        ]
        assets['notes_p2'] = [
            new Tone.Player("./assets/sounds/Scena_3/p2_note/p2_note_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p2_note/p2_note_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p2_note/p2_note_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p2_note/p2_note_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p2_note/p2_note_5.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_3/p2_note/p2_note_6.wav").toDestination()
        ]
        assets['atoms'] = [
            new Tone.Player("./assets/sounds/Scena_4/atoms/atoms_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_4/atoms/atoms_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_4/atoms/atoms_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_4/atoms/atoms_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_4/atoms/atoms_5.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_4/atoms/atoms_6.wav").toDestination()
        ]
        assets['crescendo_tremore'] = new Tone.Player("./assets/sounds/Scena_5/Crescendo_tremore.wav").toDestination()
        assets['crashes'] = [
            new Tone.Player("./assets/sounds/Scena_6/Smashes/Smash_1.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_6/Smashes/Smash_2.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_6/Smashes/Smash_3.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_6/Smashes/Smash_4.wav").toDestination(),
            new Tone.Player("./assets/sounds/Scena_6/Smashes/Smash_5.wav").toDestination()
        ]
        assets['gestting_closer'] = new Tone.Player("./assets/sounds/Scena_6/Getting_closer.wav").toDestination()
        assets['pull_away'] = new Tone.Player("./assets/sounds/Scena_6/Pull_away.wav").toDestination()
        assets['notes_p2'].forEach((el) => {
            el.volume.value = -6
        })
    }

    // SETUP --------------------------------------------------------------

    p.setup = function () {
        p.createCanvas(window.innerWidth, window.innerHeight)

        // DEFINE SCENE0 --------------------------------------------------------------

        let scene0 = {
            y_scroll: -30,
            title_showing: true,
            counter: 0,
            someone: assets.someone,
            hi_bubble: assets.hi_bubble,
            hi_bubble_showing: false,
            someone_size: 18,
            transition_interval: 2000,
            transitioning: false,
            transition_buffer: p.createGraphics(p.width, p.height),

            draw: function () {
                p.background(250)
                p.push()
                if (logged) {
                    this.counter += 5 * p.deltaTime / 50
                }
                if (this.title_showing) {
                    p.push()
                    p.imageMode(p.CENTER)
                    wr = (p.width / assets['title'].width)
                    wh = (p.height / assets['title'].height)
                    scale_ratio = wr > wh ? wr : wh

                    p.tint(255, p.map(this.counter, 100, 355, 255, 0, true))
                    if (this.counter > 355) {
                        this.title_showing = false
                        assets.bg_scene_0.loop = true
                        assets.bg_scene_0.start()
                    }
                    p.image(assets['title'], p.width / 2, p.height / 2, assets['title'].width * scale_ratio, assets['title'].height * scale_ratio)
                    p.pop()
                } else {
                    p.translate(0, this.y_scroll)
                    p.textAlign(p.CENTER)
                    p.textFont(assets.jacquesFrancois)
                    p.textSize(60)
                    p.text("1", p.width / 2, p.height - 160)
                    p.textSize(14)
                    p.text("You’re alone here. And you’ll be alone,\neventually.\nNo big deal.\nYou already knew it. We all come to the world alone.\nYou’re the only citizen of your head.\nYay, the major.\nPopulation of one.\nYou sleep with your\nthoughts,\neat breakfast with your thoughts,\nwalk with your thoughts and...suddenly.", p.width / 2, p.height)
                    p.text("There's ", p.floor(p.width / 2), p.height + 220)
                    p.textFont(assets.jacquesFrancoisShadow)
                    p.textSize(18)
                    p.push()
                    p.textSize(this.someone_size)
                    p.fill(0, 0, 250)
                    p.text("SOMEONE", p.floor(p.width / 2), p.height + 220 + 20)
                    if (this.hi_bubble_showing) {
                        p.image(this.hi_bubble, p.width / 2 + p.textWidth("SOMEONE") / 2 + 2, p.height + 220 - 20, this.hi_bubble.width * 0.5, this.hi_bubble.height * 0.5)
                    }
                    p.stroke(0, 0, 250)
                    p.line(p.width / 2 - p.textWidth("SEMEONE") / 2, p.height + 220 + 20 + 4, p.width / 2 + p.textWidth("SEMEONE") / 2, p.height + 220 + 20 + 4)
                    p.pop()
                    p.push()
                    p.textFont(assets.jacquesFrancois)
                    p.textSize(14)
                    if (scenes[0].transitioning) {
                        p.text("A lot of someone, to be fair.\nIt’s fucking crowded.\n", p.width / 2, p.height + 224 + 20 + 30)
                    }
                    p.pop()
                }

                p.pop()
                p.image(this.transition_buffer, 0, 0, p.width, p.height)
            },
            mousePressed: function () {

                // If title fade out done
                if (!this.title_showing) {

                    // On "SOMEONE" button press
                    if (p.mouseX > (p.width / 2 - p.textWidth("SOMEONE"))
                        && p.mouseX < (p.width / 2 + p.textWidth("SOMEONE"))
                        && p.mouseY > (p.height + 220 + 5 + this.y_scroll)
                        && p.mouseY < (p.height + 220 + 25 + this.y_scroll)) {

                        // Start transition to next scene
                        _connection.send({ type: "someonetouch" })
                        this.someone_size = 14
                        setTimeout(() => this.someone_size = 18, 200)
                        if (!scenes[0].transitioning) {
                            scenes[0].transitioning = true
                            setTimeout(() => {
                                assets.hi_crescendo.start()
                            }, 3000)
                            setTimeout(() => {
                                scenes[0].transition()
                                setTimeout(() => {
                                    scene_number = 1
                                    assets.bg_scene_1.loop = true
                                    assets.bg_scene_1.start()
                                    assets.bg_scene_0.stop()
                                    assets.hi_crescendo.stop()
                                }, 10000)
                            }, 1000)

                        }

                    }
                }

            },
            mouseDragged: function (movementY) {
                if (!this.title_showing) {
                    this.y_scroll += movementY
                }
            },
            transition: function () {
                setTimeout(() => {
                    scenes[0].transition_interval *= 0.7
                    let random_scale = p.random(0.5, 2)
                    if (p.random(0, 1) > 0.5) {
                        scenes[0].transition_buffer.image(scenes[0].hi_bubble, p.random(0, p.width), p.random(0, p.height), scenes[0].hi_bubble.width * 0.5 * random_scale, scenes[0].hi_bubble.height * 0.5 * random_scale)
                    } else {
                        scenes[0].transition_buffer.image(scenes[0].someone, p.random(0, p.width), p.random(0, p.height), scenes[0].someone.width * 0.5 * random_scale, scenes[0].someone.height * 0.5 * random_scale)
                    }
                    if (scene_number == 0) {
                        scenes[0].transition()
                    }
                }, scenes[0].transition_interval)
            }
        }
        scenes.push(scene0)

        // DEFINE SCENE1 --------------------------------------------------------------
        scene1 = {
            partner_flashlight_on: false,
            partner_flashlight_coords: [0, 0],
            insect_rotation: 0,
            insect_animation_startframe: 0,
            insect_animating: false,
            insects_coords: [p.textWidth("matched.Will your souls be?") + 40, 8 * 18 + 34 + 50],
            crowdShown: false,
            draw: function () {
                p.push()

                p.background(0)
                if (this.insect_animating && (this.insects_coords[1] < p.height)) {
                    this.insects_coords[1] += 1 * p.deltaTime / 50
                }
                if (p.touches.length > 0) {
                    p.image(assets.flashlight, p.mouseX - assets.flashlight.width / 2, p.mouseY - assets.flashlight.height * 1.5, assets.flashlight.width, assets.flashlight.height)
                }
                if (this.partner_flashlight_on) {
                    p.image(assets.flashlight, this.partner_flashlight_coords[0] - assets.flashlight.width / 2, this.partner_flashlight_coords[1] - assets.flashlight.height * 1.5, assets.flashlight.width, assets.flashlight.height)
                }
                p.textFont(assets.jacquesFrancois)
                p.textSize(14)
                p.text("Chill, you’re still alone. At the same time, you’re not.\nLook around.\nThere’s someone.\nYes, you look great. That’s not the point, anyway.\nThis someone wants to play. Do you want to play?\b\nDon’t fix your hair. It's not a blind date.\nDon't over react.\nYour devices are already matched.\nWill your souls be?", p.textWidth("aa"), 50)
                p.fill(0)
                p.image(assets.insect, this.insects_coords[0], this.insects_coords[1], assets.insect.width * 0.15, assets.insect.height * 0.15)
                if (this.insect_animating) {
                    p.text("You feel something creeping under your clothes.\nThis doubt is crawling up your back.", p.textWidth("aa"), 126 * 3.5)
                }

                if (!this.crowdShown && this.insect_animating) {
                    assets['insectCrowd'] = p.createImg("./assets/bugCrowd.gif", "")
                    assets.insectCrowd.position(0, 126 * 1.3)
                    assets.insectCrowd.size(window.innerWidth, window.innerWidth)
                    this.crowdShown = true
                }
                p.rect(0, 126 * 1.3 + window.innerWidth, p.width, p.height)
                p.pop()
            },
            mousePressed: function () {

            }
        }
        scenes.push(scene1)

        // DEFINE SCENE2 --------------------------------------------------------------

        scene2 = {
            transitioning: false,
            host_random_word: RiTa.pluralize(RiTa.randomWord("nn")),
            guest_random_word: RiTa.pluralize(RiTa.randomWord("nn")),
            draw: function () {
                p.push()
                p.textSize(16)
                p.background(250)
                p.fill(0)
                p.textFont(assets.jacquesFrancois)
                p.stroke(0)
                p.text(!amihost ? "They think" : "You think", p.textWidth("aa"), p.height / 4 - 16)
                p.noStroke()
                p.text("doubt goes well with ", p.textWidth("aa"), p.height / 4)
                p.textSize(16)
                p.textFont(assets.jacquesFrancoisShadow)
                if (amihost) {
                    p.fill(0, 0, 250)
                } else {
                    p.fill(0)
                }
                p.text(this.host_random_word, p.textWidth("doubt goes well with "), p.height / 4)
                if (amihost) {
                    p.stroke(0, 0, 250)
                } else {
                    p.noStroke()
                }
                p.line(p.textWidth("doubt goes well with "), p.height / 4 + 6, p.textWidth(this.host_random_word) + p.textWidth("doubt goes well with "), p.height / 4 + 6)
                p.textFont(assets.jacquesFrancois)
                p.fill(0)
                p.stroke(0)
                p.text(amihost ? "They think" : "You think", p.textWidth("aa"), p.height * 3 / 4 - 16)
                p.noStroke()
                p.text("doubt goes well with ", p.textWidth("aa"), p.height * 3 / 4)
                p.textFont(assets.jacquesFrancoisShadow)
                if (!amihost) {
                    p.fill(0, 0, 250)
                } else {
                    p.fill(0)
                }
                p.text(this.guest_random_word, p.textWidth("doubt goes well with "), p.height * 3 / 4)
                if (!amihost) {
                    p.stroke(0, 0, 250)
                } else {
                    p.noStroke()
                }
                p.line(p.textWidth("doubt goes well with "), p.height * 3 / 4 + 6, p.textWidth(this.guest_random_word) + p.textWidth("doubt goes well with "), p.height * 3 / 4 + 6)
                p.pop()
            },
            mousePressed: function () {
                if (amihost &&
                    p.mouseX > p.textWidth("doubt goes well with ") &&
                    p.mouseX < (p.textWidth(this.host_random_word) * 1.05 + p.textWidth("doubt goes well with ")) &&
                    p.mouseY > p.height / 4 - 12 &&
                    p.mouseY < p.height / 4 + 16) {
                    assets.pages_turn[Math.floor(Math.random() * assets.pages_turn.length)].start()
                    emitRandomWord()
                } else if (!amihost &&
                    p.mouseX > p.textWidth("doubt goes well with ") &&
                    p.mouseX < (p.textWidth(this.guest_random_word) * 1.05 + p.textWidth("doubt goes well with ")) &&
                    p.mouseY > p.height * 3 / 4 - 12 &&
                    p.mouseY < p.height * 3 / 4 + 16) {
                    assets.pages_turn[Math.floor(Math.random() * assets.pages_turn.length)].start()
                    emitRandomWord()
                }
            }
        }
        scenes.push(scene2)

        // DEFINE SCENE 3 --------------------------------------------------------------

        scene3 = {
            draw: function () {
                p.push()
                p.textFont(assets.jacquesFrancois)
                p.textAlign(p.CENTER)
                p.textSize(14)
                p.text("This is how it works:\nsometimes you just can’t hide your thoughts.", p.width / 2, p.height / 2)
                p.pop()
            },
            mousePressed: function () {

            }
        }
        scenes.push(scene3)

        // DEFINE SCENE 4 --------------------------------------------------------------

        scene4 = {
            x_scroll: 0,
            ending: false,
            colors: {
                host: p.color(240, 128, 128),
                guest: p.color(255, 218, 185)
            },
            main_phrase: {
                text: "Now that you both have taken a step, it’s time to explore. It’s exciting, isn’it? When you get to know someone. The challenge of being explored and of exploring. Collecting all the hints that you’re given. ",
                position: { x: 0, y: p.height / 2 }
            },
            second_phrase: {
                text: "Discovery is like being poured by an incessant shower of innumerable atoms. Virginia Woolf would say that about life. Discovery is life itself.",
                position: { x: 0, y: p.height / 2 }
            },
            dots: { host: [], guest: [] },
            animatedDots: { host: [], guest: [] },
            setup: function () {
                p.textFont(assets.jacquesFrancois)
                p.textSize(16)
                this.main_phrase.position.x = (p.width - p.textWidth(this.main_phrase.text.substr(0, 10)) - p.textWidth("o") / 2)
                this.second_phrase.position.x = (p.textWidth(this.main_phrase.text) + this.main_phrase.position.x)
            },
            draw: function () {
                p.push()
                p.translate(this.x_scroll, 0)
                p.noStroke()
                this.dots.host.forEach((dot, index) => {
                    p.fill(this.colors.host)
                    p.ellipse(this.main_phrase.position.x + dot.x, this.main_phrase.position.y + dot.y, 14, 14)
                    if (index > 0) {
                        p.stroke(this.colors.host)
                        p.line(this.main_phrase.position.x + this.dots.host[index - 1].x, this.main_phrase.position.y + this.dots.host[index - 1].y, this.main_phrase.position.x + dot.x, this.main_phrase.position.y + dot.y)
                    }
                })
                p.noStroke()
                this.dots.guest.forEach((dot, index) => {
                    p.fill(this.colors.guest)
                    p.ellipse(this.main_phrase.position.x + dot.x, this.main_phrase.position.y + dot.y, 14, 14)
                    if (index > 0) {
                        p.stroke(this.colors.guest)
                        p.line(this.main_phrase.position.x + this.dots.guest[index - 1].x, this.main_phrase.position.y + this.dots.guest[index - 1].y, this.main_phrase.position.x + dot.x, this.main_phrase.position.y + dot.y)
                    }
                })
                p.noStroke()
                p.fill(0)
                p.text(this.main_phrase.text, this.main_phrase.position.x, this.main_phrase.position.y)
                p.stroke(0)
                p.text(this.second_phrase.text, this.second_phrase.position.x, this.second_phrase.position.y)
                p.noStroke()
                this.animatedDots.host.slice().reverse().forEach((dot) => {
                    dot.animate()
                    if (dot.size < 0) {
                        this.animatedDots.host.splice(this.animatedDots.host.indexOf(dot), 1)
                    }
                    dot.draw(this.colors.host)
                })
                this.animatedDots.guest.slice().reverse().forEach((dot) => {
                    dot.animate()
                    if (dot.size < 0) {
                        this.animatedDots.guest.splice(this.animatedDots.guest.indexOf(dot), 1)
                    }
                    dot.draw(this.colors.guest)
                })
                p.pop()
            },
            mousePressed: function () {

            }
        }
        scenes.push(scene4)

        // DEFINE SCENE 5 --------------------------------------------------------------

        scene5 = {
            counter: 0,
            shaking: false,
            shake_factor: 20,
            shake_begin: 1400,
            shake_end: 1800,
            setup: function () {
                p.textFont(assets.jacquesFrancois)
                p.textSize(14)
            },
            draw: function () {
                this.counter += 5 * p.deltaTime / 50
                p.push()
                if (this.shaking == true) {
                    p.translate(p.noise(this.counter, 1) * p.map(this.counter, this.shake_begin, this.shake_end, 0, this.shake_factor, true), p.noise(this.counter, 2) * p.map(this.counter, this.shake_begin, this.shake_end, 0, this.shake_factor, true))
                }
                p.textAlign(p.CENTER)
                p.fill(0, p.map(this.counter, 0, 255, 0, 255, true))
                p.text("Where does discovery lead?", p.width / 2, p.height / 2)
                p.fill(0, p.map(this.counter, 400, 655, 0, 255, true))
                p.text("Epiphany", p.width / 2, p.height / 2 + 14 + 10)
                p.fill(0, p.map(this.counter, 700, 955, 0, 255, true))
                p.text("A sudden spiritual manifestation", p.width / 2, p.height / 2 + 28 + 20)
                p.fill(0, p.map(this.counter, 1000, 1255, 0, 255, true))
                p.text("That hits you.", p.width / 2, p.height / 2 + 42 + 30)
                p.fill(0, p.map(this.counter, 1300, 1555, 0, 255, true))
                p.text("Shakes you.", p.width / 2, p.height / 2 + 56 + 40)
                if ((this.counter > this.shake_begin) && (this.counter < this.shake_end) && this.shaking == false) {
                    this.shaking = true

                }
                if (this.counter > this.shake_end && this.shaking == true) {
                    this.shaking = false
                    scenes[6].setup()
                    scene_number = 6
                    assets.crescendo_tremore.stop()
                }
                p.pop()
            },
            mousePressed: function () {

            }
        }
        scenes.push(scene5)

        // DEFINE SCENE 6 --------------------------------------------------------------

        scene6 = {
            counter: 0,
            setup: function () {
                p.textFont(assets.jacquesFrancois)
                p.textSize(14)
            },
            draw: function () {
                this.counter += 5 * p.deltaTime / 50
                if (this.counter > 655) {
                    scenes[7].setup()
                    scene_number = 7
                }
                p.push()
                p.background(0)
                p.fill(250)
                p.textAlign(p.CENTER)
                p.text("And then knowledge.", p.width / 2, p.height / 2)
                p.pop()
            },
            mousePressed: function () {

            }
        }
        scenes.push(scene6)

        // DEFINE SCENE 7 --------------------------------------------------------------

        scene7 = {
            animating_crashing: false,
            animating_closer: false,
            animating_apart: false,
            counter: 0,
            crashing_duration: 500,
            phrases: [
                new AnimatedText("Bonds get stronger when they're stressed", false),
                new AnimatedText("Getting closer", false),
                new AnimatedText("pushed away,"),
                new AnimatedText("being apart", false),
                new AnimatedText("blending with each other again.", false),
                new AnimatedText("Connecting", false),
                new AnimatedText("crashing", true),
                new AnimatedText("Building trust."),
                new AnimatedText("It’s hard and pointless,", false),
                new AnimatedText("When you trust,", false),
                new AnimatedText("To withstand the desire", false),
                new AnimatedText("of digging.", false)
            ],
            setup: function () {
                p.textFont(assets.jacquesFrancois)
                p.textSize(14)
                this.phrases.forEach((el, index) => {
                    el.position.y = p.height / 2 + index * (10 + 14) - 147
                })
            },
            draw: function () {
                if (this.animating_crashing && (this.counter < this.crashing_duration)) {
                    this.phrases.forEach((el) => {
                        if (!el.isButton) {
                            el.position.x = p.map(this.counter, 0, this.crashing_duration, el.initPosition.x, el.target.x, true)
                            el.position.y = p.map(this.counter, 0, this.crashing_duration, el.initPosition.y, el.target.y, true)
                        }
                    })
                    this.counter += (5 * p.deltaTime / 50)
                    if (this.counter > this.crashing_duration) {
                        this.animating_crashing = false
                    }
                }
                if (this.animating_closer && (this.counter < this.crashing_duration)) {
                    this.phrases.forEach((el) => {
                        if (!el.isButton) {
                            el.position.x = p.map(this.counter, 0, this.crashing_duration, el.initPosition.x, el.target.x, true)
                            el.position.y = p.map(this.counter, 0, this.crashing_duration, el.initPosition.y, el.target.y, true)
                        }
                    })
                    this.counter += (5 * p.deltaTime / 50)
                    if (this.counter > this.crashing_duration) {
                        this.animating_closer = false
                    }
                }
                if (this.animating_apart && (this.counter < this.crashing_duration)) {
                    this.phrases.forEach((el) => {
                        if (!el.isButton) {
                            el.position.x = p.map(this.counter, 0, this.crashing_duration, el.initPosition.x, el.target.x, true)
                            el.position.y = p.map(this.counter, 0, this.crashing_duration, el.initPosition.y, el.target.y, true)
                        }
                    })
                    this.counter += (5 * p.deltaTime / 50)
                    if (this.counter > this.crashing_duration) {
                        this.animating_apart = false
                    }
                }
                p.push()
                p.textAlign(p.CENTER)
                this.phrases.forEach((el) => {
                    if (el.isButton) {
                        p.fill(0, 0, 250)
                        p.stroke(0, 0, 250)
                    } else {
                        p.fill(0)
                        p.noStroke()
                    }
                    p.text(el.text, el.position.x, el.position.y)
                })
                p.pop()
            },
            mousePressed: function () {
                this.phrases.forEach((el, index) => {
                    if (el.isButton) {
                        if (scene_number == 7 && !scenes[7].ending) {
                            scenes[7].ending = true
                            _connection.send({ type: "endScene7", partner_id: partner_id })
                            setTimeout(() => {
                                scene_number = 8
                                scenes[8].setup()
                            }, 10000)
                        }
                        if (el.mousePressed(p.mouseX, p.mouseY)) {
                            if (index == 6) {
                                // animation crashing
                                assets.crashes[Math.floor(Math.random() * assets.crashes.length)].start()
                                this.counter = 0
                                this.animating_crashing = true
                                this.animating_closer = false
                                this.phrases[1].isButton = true
                                this.phrases[3].isButton = true
                                this.phrases.forEach((el) => {
                                    el.initPosition = el.position
                                    el.target = { x: p.random(0, p.width), y: p.random(0, p.height) }
                                })
                            } else if (index == 1) {
                                // animation getting closer
                                assets.gestting_closer.start()
                                this.counter = 0
                                this.animating_closer = true
                                this.animating_crashing = false
                                this.phrases.forEach((el) => {
                                    el.initPosition = el.position
                                    el.target = { x: p.width / 2, y: p.height / 2 }
                                })
                            } else if (index == 3) {
                                // animation pull away
                                assets.pull_away.start()
                                this.counter = 0
                                this.animating_closer = false
                                this.animating_crashing = false
                                this.animating_apart = true
                                this.phrases.forEach((el) => {
                                    el.initPosition = el.position
                                    el.target = { x: (p.random(0, 1) > 0.5) ? (- p.width) : p.width, y: (p.random(0, 1) > 0.5) ? (p.height + 50) : (-50) }
                                })
                            }

                        }
                    }
                })
            }
        }
        scenes.push(scene7)

        // DEFINE SCENE 8 --------------------------------------------------------------

        scene8 = {
            counter: 0,
            partner_soul_on: false,
            partner_soul_coords: { x: 0, y: 0 },
            soul_size: 30,
            setup: function () {
                p.background(250)
                p.textFont(assets.jacquesFrancois)
                p.textSize(14)
            },
            draw: function () {
                this.counter += 1 * p.deltaTime / 50
                p.push()
                p.background(0)
                p.noStroke()
                p.fill(0, 0, 0, 200)
                p.rect(0, 0, p.width, p.height)
                p.fill(250, 250, 250, 200)
                if (p.touches.length > 0) {
                    p.ellipse(p.mouseX, p.mouseY, this.soul_size, this.soul_size)
                }
                if (this.partner_soul_on) {
                    p.ellipse(this.partner_soul_coords.x, this.partner_soul_coords.y, this.soul_size, this.soul_size)
                }
                p.textAlign(p.CENTER)
                p.text("Vulnerability is empowering.\nMerging is overwhelming.\nAbandoning yourself to the other is freeing.\nEven if you turn out to be alone, in the end,\nThe deep understanding of the other,\nMade the cruise worth it\n\n\n\nDon’t wait.\n\n\n\nGo.\n\n\n\nLet them know you realized.\n\n\n\n\n\n\n\n\n\n\n\nTHE END\n\n\n\n\n\n\n\n\n\n\nDirection and Concept:\nRuben Gandus\nLorenzo Micozzi Ferri\n\n\nWritten by:\nLivia Chiriatti\n\n\nMusic and Sound Design:\nFilippo Rabottini\n\n\nDeveloped by:\nLorenzo Micozzi Ferri",
                    p.width / 2, p.height - p.map(this.counter, 100, 500, 0, p.height / 2))
                p.pop()
            },
            mousePressed: function () {

            }
        }
        scenes.push(scene8)

        var el = document.getElementById("defaultCanvas0");
        //var mc = new Hammer(el);

        // // Tap event callback
        // el.addEventListener("click", function (ev) {
        //     if (!audioStarted) {
        //         Tone.start()
        //         audioStarted = true
        //     }
        //     if (scene_number == 0) {
        //         for (const clickable of clickables) {
        //             if (p.mouseX > clickable.x && p.mouseX < (clickable.x + clickable.width) && p.mouseY > clickable.y && p.mouseY < (clickable.y + clickable.height)) {
        //                 clickable.mousePressed()
        //                 return
        //             }
        //         }
        //     }
        //     if (!logged) {
        //         if (!gotCode) {
        //             landing_page.mousePressed()
        //         } else {
        //             code_page.mousePressed()
        //         }
        //     } else {
        //         scenes[scene_number].mousePressed()
        //     }
        // });


        logged = false
        gotCode = false
        p.textSize(fontHeight)

        //create fullscreenToggle
        fullscreenToggle = {
            image: assets.fullscreen_icon,
            x: 0,
            y: 0,
            width: 256,
            height: 256,
            position: function (_x, _y) {
                this.x = _x
                this.y = _y
            },
            size: function (_width, _height) {
                this.width = _width
                this.height = _height
            },
            draw: function () {
                p.push()
                p.tint(255, 200)
                p.image(this.image, this.x, this.y, this.width, this.height)
                p.pop()
            },
            mousePressed: function () {
                p.fullscreen(true)
                p.resizeCanvas(window.innerWidth, window.innerHeight)
                p.redraw()

            }
        }

        // DEFINE LANDING PAGE --------------------------------------------------------------
        landing_page = {
            instructions_showing: true,
            draw: function () {
                if (this.instructions_showing) {
                    // instructions
                    p.background(255)
                    p.push()
                    p.imageMode(p.CENTER)
                    wr = (p.width / assets['instructions'].width)
                    wh = (p.height / assets['instructions'].height)
                    scale_ratio = (wr > wh ? wh : wr) * 0.8
                    p.image(assets['instructions'], p.width / 2, p.height / 2, assets['instructions'].width * scale_ratio, assets['instructions'].height * scale_ratio)
                    p.pop()
                } else {
                    p.push()
                    p.textFont(assets.jacquesFrancois);
                    p.textAlign(p.CENTER)
                    p.text('Waiting for your partner', p.width / 2, p.height / 2);
                    p.pop()
                }
            },
            mousePressed: function () {

                this.instructions_showing = false
                if (partner_ready) {
                    _connection.send({ type: "letsbegin" })
                    logged = true
                    scenes[0].counter = 0
                } else {
                    _connection.send({ type: "imready" })
                }

            }
        }

        // push clickables elements
        clickables.push(fullscreenToggle)
        // size and position fullscreenToggle
        fullscreenToggle.size(p.floor(p.height * 0.05), p.floor(p.height * 0.05))
        fullscreenToggle.position(p.width - fullscreenToggle.width, p.height - fullscreenToggle.height)


    }


    // DRAW //

    p.draw = function () {
        //refresh background
        if (scene_number != 8) {
            p.background(250)
        }
        if (!logged) {
            if (gotCode) {
                code_page.draw()
            } else {
                landing_page.draw()
            }
        } else {
            //render current scene
            scenes[scene_number].draw()
        }
        //draws fullscreen button when not in fullscreen mode
        if (!p.fullscreen() && scene_number == 0) {
            fullscreenToggle.draw()
        }
    }


    // TOUCH CALLBACKS

    // Touch move
    window.addEventListener("touchmove", function (ev) {
        if (scene_number == 0 && !scenes[0].title_showing) {
            scenes[0].y_scroll = y_scroll_begin + (ev.touches[0].clientY - touchBegin.clientY)
        }
        else if (scene_number == 1) {
            _connection.send({ type: "moveFlashlight", touchX: ev.touches[0].clientX, touchY: ev.touches[0].clientY, partner_id: partner_id })
            if (!scenes[1].insect_animating) {
                if (ev.touches[0].clientX > (scenes[1].insects_coords[0] - assets.flashlight.width / 2)
                    && ev.touches[0].clientX < (scenes[1].insects_coords[0] + assets.flashlight.width / 2)
                    && (ev.touches[0].clientY - assets.flashlight.height * 1.15) > (scenes[1].insects_coords[1] - assets.flashlight.height / 2)
                    && (ev.touches[0].clientY - assets.flashlight.height * 1.15) < (scenes[1].insects_coords[1] + assets.flashlight.height / 2)) {
                    _connection.send({ type: "flashlightOnInsect", partner_id: partner_id })
                    setTimeout(() => {
                        if (!scenes[1].insect_animating) {
                            assets.scarafaggi_loop.loop = true
                            assets.scarafaggi_loop.start()
                            setTimeout(() => {
                                scene_number = 2
                                assets.scarafaggi_loop.stop()
                                assets.bg_scene_1.stop()
                                assets.bg_scene_2.loop = true
                                assets.bg_scene_2.start()
                                //emitRandomWord()
                                assets.insectCrowd.hide()
                            }, 20000)
                            scenes[1].insect_animating = true
                        }
                    }, 500)
                }
            }
        } else if (scene_number == 4) {
            scenes[4].x_scroll = x_scroll_begin + (ev.touches[0].clientX - touchBegin.clientX)
        } else if (scene_number == 8) {
            _connection.send({ type: "moveSoul", touchX: ev.touches[0].clientX, touchY: ev.touches[0].clientY, partner_id: partner_id })
        }
        ev.preventDefault();
    }, { passive: false });

    // Touch start
    window.addEventListener("touchstart", function (ev) {
        if (!audioStarted) {
            Tone.start()
            audioStarted = true
        }
        if (scene_number == 0) {
            for (const clickable of clickables) {
                if (p.mouseX > clickable.x && p.mouseX < (clickable.x + clickable.width) && p.mouseY > clickable.y && p.mouseY < (clickable.y + clickable.height)) {
                    clickable.mousePressed()
                    return
                }
            }
        }
        if (!logged) {
            if (!gotCode) {
                landing_page.mousePressed()
            } else {
                code_page.mousePressed()
            }
        } else {
            scenes[scene_number].mousePressed()
        }
        if (scene_number == 0 && !scenes[0].title_showing) {
            touchBegin = ev.touches[0]
            y_scroll_begin = scenes[0].y_scroll
        }
        else if (scene_number == 1) {
            _connection.send({ type: "onFlashlight", touchX: ev.touches[0].clientX, touchY: ev.touches[0].clientY, partner_id: partner_id })
            assets.clicks_torcia[Math.floor(Math.random() * assets.clicks_torcia.length)].start()
        } else if (scene_number == 4) {
            touchBegin = ev.touches[0]
            x_scroll_begin = scenes[4].x_scroll
            let relative_x = (ev.touches[0].clientX - scenes[4].main_phrase.position.x - x_scroll_begin)
            let relative_y = (ev.touches[0].clientY - scenes[4].main_phrase.position.y)
            if (amihost) {
                if (scenes[4].dots.host.length < 1 || (relative_x > scenes[4].dots.host[scenes[4].dots.host.length - 1].x)) {
                    scenes[4].dots.host.push({ x: relative_x, y: relative_y })
                    assets.notes_p1[Math.floor(Math.random() * assets.notes_p1.length)].start()
                    if (relative_x > p.textWidth(scenes[4].main_phrase.text)) {
                        scenes[4].animatedDots.host.push(new AnimatedDot(relative_x, relative_y, p.frameCount))
                        assets.atoms[Math.floor(Math.random() * assets.atoms.length)].start()
                    }
                }
            } else {
                if (scenes[4].dots.guest.length < 1 || (relative_x > scenes[4].dots.guest[scenes[4].dots.guest.length - 1].x)) {
                    scenes[4].dots.guest.push({ x: relative_x, y: relative_y })
                    assets.notes_p2[Math.floor(Math.random() * assets.notes_p1.length)].start()
                    if (relative_x > p.textWidth(scenes[4].main_phrase.text)) {
                        scenes[4].animatedDots.guest.push(new AnimatedDot(relative_x, relative_y, p.frameCount))
                        assets.atoms[Math.floor(Math.random() * assets.atoms.length)].start()
                    }
                }
            }
            _connection.send({ type: "onNewDot", touchX: relative_x, touchY: relative_y, partner_id: partner_id })
        } else if (scene_number == 8) {
            _connection.send({ type: "onSoul", touchX: ev.touches[0].clientX, touchY: ev.touches[0].clientY, partner_id: partner_id })
        }
        ev.preventDefault();
    }, { passive: false });


    // Touch end
    window.addEventListener("touchend", function (ev) {
        if (scene_number == 1) {
            _connection.send({ type: "offFlashlight", partner_id: partner_id })
        }
        if (scene_number == 4) {
            if ((scenes[4].second_phrase.position.x + scenes[4].x_scroll + p.textWidth(scenes[4].second_phrase.text)) < p.width && scenes[4].ending == false) {
                _connection.send({ type: "endScene4", partner_id: partner_id })
                scenes[4].ending = true
                setTimeout(() => {
                    if (scene_number == 4) {
                        scene_number = 5
                        assets.crescendo_tremore.start()
                        scenes[5].setup()
                    }
                }, 10000)
            }
        } else if (scene_number == 8) {
            _connection.send({ type: "offSoul", partner_id: partner_id })
        }
        ev.preventDefault();
    }, { passive: false });

    class AnimatedDot {
        constructor(_x, _y, _initFrame) {
            this.x = _x
            this.y = _y
            this.initFrame = _initFrame
            this.dots = [{ position: { x: this.x, y: this.y }, vel: { x: 1, y: 1 } }, { position: { x: this.x, y: this.y }, vel: { x: -1, y: -1 } }, { position: { x: this.x, y: this.y }, vel: { x: -1, y: 1 } }, { position: { x: this.x, y: this.y }, vel: { x: 1, y: -1 } }]
            this.size = 20
        }

        animate() {
            this.dots.forEach((dot, index) => {
                dot.position.x += dot.vel.x * p.deltaTime / 50
                dot.position.y += dot.vel.y * p.deltaTime / 50
            })
            this.size -= 0.5 * p.deltaTime / 50
        }

        draw(_color) {
            let noiseFrame = p.frameCount
            this.dots.forEach((dot, index) => {
                p.push()
                p.fill(_color)
                p.translate(this.x + scenes[4].main_phrase.position.x, this.y + scenes[4].main_phrase.position.y)
                p.rotate(p.noise(this.initFrame, noiseFrame * 0.005 + index) * 3)
                p.translate(- this.x - scenes[4].main_phrase.position.x, - this.y - scenes[4].main_phrase.position.y)
                p.ellipse(scenes[4].main_phrase.position.x + dot.position.x, scenes[4].main_phrase.position.y + dot.position.y, this.size, this.size)
                p.pop()
            })
        }
    }

    class AnimatedText {
        constructor(_text, _isButton) {
            this.text = _text
            this.isButton = _isButton
            this.position = { x: p.width / 2, y: 0 }
            this.initPosition = { x: p.width / 2, y: 0 }
            this.target = { x: p.width / 2, y: p.height / 2 }
        }
        mousePressed(_x, _y) {
            if (
                _x > (this.position.x - p.textWidth(this.text) / 2) &&
                _x < (this.position.x + p.textWidth(this.text) / 2) &&
                _y > (this.position.y - 14 / 2) &&
                _y < (this.position.y + 14 / 2)
            ) {
                return true
            }
        }
    }

    p.windowResized = function () {
        //size and position fullscreenToggle
        fullscreenToggle.size(p.floor(p.height * 0.05), p.floor(p.height * 0.05))
        fullscreenToggle.position(p.width - fullscreenToggle.width, p.height - fullscreenToggle.height)
        p.redraw()
    }

}

